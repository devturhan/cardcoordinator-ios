//
//  SequentialBasedCardCoordinatorTests.swift
//  CardCoordinatorTests
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import XCTest
@testable import CardCoordinator
class SequentialBasedCardCoordinatorTests: XCTestCase {
    
    var sequentialBasedCardCoordinator:SequentialBasedCardCoordinator!
    
    override func setUp() {
        super.setUp()
        self.sequentialBasedCardCoordinator = SequentialBasedCardCoordinator()
    }
    
    override func tearDown() {
        self.sequentialBasedCardCoordinator = nil
        super.tearDown()
    }
    
    func testCardCoordinatorWithEmptyArray() {
        let cards: [Card] = []
        let result = sequentialBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = []
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioOne() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .jack)
        let card3 = Card(type: .clubs, value: .three)
        let card4 = Card(type: .clubs, value: .four)
        let card5 = Card(type: .spades, value: .ace)
        let card6 = Card(type: .diamond, value: .four)
        let card7 = Card(type: .hearts, value: .queen)
        let card8 = Card(type: .diamond, value: .king)
        let card9 = Card(type: .spades, value: .eight)
        let card10 = Card(type: .clubs, value: .two)
        let card11 = Card(type: .diamond, value: .five)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = sequentialBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card1,card10,card3,card4,card6,card11,card8,card5,card9,card2,card7]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioTwo() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .jack)
        let card3 = Card(type: .clubs, value: .three)
        let card4 = Card(type: .clubs, value: .four)
        let card5 = Card(type: .spades, value: .ace)
        let card6 = Card(type: .diamond, value: .four)
        let card7 = Card(type: .hearts, value: .queen)
        let card8 = Card(type: .diamond, value: .king)
        let card9 = Card(type: .spades, value: .eight)
        let card10 = Card(type: .clubs, value: .two)
        let card11 = Card(type: .diamond, value: .five)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = sequentialBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card1,card10,card3,card4,card6,card11,card8,card5,card9,card2,card7]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioThreeWithNonOrdered() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .jack)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .clubs, value: .four)
        let card5 = Card(type: .spades, value: .ace)
        let card6 = Card(type: .diamond, value: .four)
        let card7 = Card(type: .hearts, value: .queen)
        let card8 = Card(type: .diamond, value: .king)
        let card9 = Card(type: .spades, value: .eight)
        let card10 = Card(type: .hearts, value: .two)
        let card11 = Card(type: .diamond, value: .five)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = sequentialBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card6, card11, card8, card5, card3, card9, card1, card4, card2, card10, card7]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioFourWithFullOrdered() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .two)
        let card3 = Card(type: .clubs, value: .three)
        let card4 = Card(type: .clubs, value: .four)
        let card5 = Card(type: .clubs, value: .five)
        let card6 = Card(type: .clubs, value: .six)
        let card7 = Card(type: .clubs, value: .seven)
        let card8 = Card(type: .clubs, value: .eight)
        let card9 = Card(type: .clubs, value: .nine)
        let card10 = Card(type: .clubs, value: .ten)
        let card11 = Card(type: .clubs, value: .jack)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = sequentialBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioFourWithFullDifferentOrdered() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .two)
        let card3 = Card(type: .clubs, value: .three)
        let card4 = Card(type: .clubs, value: .four)
        let card5 = Card(type: .clubs, value: .five)
        let card6 = Card(type: .clubs, value: .six)
        let card7 = Card(type: .clubs, value: .seven)
        let card8 = Card(type: .clubs, value: .eight)
        let card9 = Card(type: .clubs, value: .nine)
        let card10 = Card(type: .clubs, value: .ten)
        let card11 = Card(type: .clubs, value: .jack)
        
        let cards: [Card] = [card6,card7,card8,card9,card10,card1,card2,card3,card4,card5,card11]
        let result = sequentialBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        
        XCTAssert(result == expectation)
    }
    
}
