//
//  SmartCardCoordinator.swift
//  CardCoordinatorTests
//
//  Created by Ömer Turhan on 14.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import XCTest
@testable import CardCoordinator
class SmartCardCoordinatorTests: XCTestCase {
    
    var smartCardCoordinator:SmartCardCoordinator!
    
    override func setUp() {
        super.setUp()
        smartCardCoordinator = SmartCardCoordinator()
    }
    
    override func tearDown() {
        smartCardCoordinator = nil
        super.tearDown()
    }
    
    
    func testCardCoordinatorWithScenarioOne() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .spades, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .spades, value: .four)
        let card5 = Card(type: .diamond, value: .three)
        let card6 = Card(type: .diamond, value: .four)
        let card7 = Card(type: .diamond, value: .five)
        let card8 = Card(type: .hearts, value: .ace)
        let card9 = Card(type: .diamond, value: .ace)
        let card10 = Card(type: .clubs, value: .four)
        let card11 = Card(type: .hearts, value: .four)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card5,card6,card7,card1,card2,card3,card10,card11,card4,card9,card8]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioTwo() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .spades, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .spades, value: .four)
        let card5 = Card(type: .diamond, value: .three)
        let card6 = Card(type: .diamond, value: .four)
        let card7 = Card(type: .diamond, value: .five)
        let card8 = Card(type: .hearts, value: .four)
        let card9 = Card(type: .diamond, value: .six)
        let card10 = Card(type: .clubs, value: .four)
        let card11 = Card(type: .hearts, value: .three)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation1: [Card] = [card5,card6,card7,card9,card1,card2,card3,card10,card8,card4,card11]
        let expectation2: [Card] = [card6,card7,card9,card3,card5,card11,card4,card8,card10,card1,card2]
        XCTAssert( result == expectation1 || result == expectation2 )
    }
    
    func testCardCoordinatorWithScenarioThree() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .spades, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .spades, value: .four)
        let card5 = Card(type: .diamond, value: .seven)
        let card6 = Card(type: .diamond, value: .four)
        let card7 = Card(type: .diamond, value: .five)
        let card8 = Card(type: .spades, value: .five)
        let card9 = Card(type: .diamond, value: .six)
        let card10 = Card(type: .clubs, value: .four)
        let card11 = Card(type: .hearts, value: .four)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card7,card9,card5,card1,card2,card3,card4,card8,card10,card11,card6]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioFour() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .spades, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .spades, value: .four)
        let card8 = Card(type: .spades, value: .five)
        let card5 = Card(type: .diamond, value: .ace)
        let card6 = Card(type: .diamond, value: .two)
        let card7 = Card(type: .clubs, value: .ace)
        let card9 = Card(type: .clubs, value: .two)
        let card10 = Card(type: .hearts, value: .jack)
        let card11 = Card(type: .hearts, value: .king)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card3,card4,card8,card1,card5,card7,card2,card6,card9,card10,card11]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioFiveOnlyTypeBased() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .hearts, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .hearts, value: .four)
        let card8 = Card(type: .spades, value: .five)
        let card5 = Card(type: .diamond, value: .ace)
        let card6 = Card(type: .diamond, value: .two)
        let card7 = Card(type: .clubs, value: .ace)
        let card9 = Card(type: .clubs, value: .two)
        let card10 = Card(type: .hearts, value: .jack)
        let card11 = Card(type: .hearts, value: .king)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card1,card5,card7,card2,card6,card9,card3,card4,card8,card10,card11]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioSix() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .spades, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .spades, value: .four)
        let card5 = Card(type: .diamond, value: .ace)
        let card6 = Card(type: .clubs, value: .ace)
        let card7 = Card(type: .hearts, value: .jack)
        
        let cards: [Card] = [card5,card2,card3,card4,card1,card6,card7]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card2,card3,card4,card5,card6,card1,card7]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioSeven() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .spades, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .spades, value: .four)
        let card5 = Card(type: .diamond, value: .ace)
        let card6 = Card(type: .clubs, value: .ace)
        let card7 = Card(type: .hearts, value: .four)
        let card8 = Card(type: .diamond, value: .four)
        
        let cards: [Card] = [card5,card2,card3,card4,card1,card6,card7,card8]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card1,card2,card3,card8,card7,card4,card5,card6]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioEight() {
        let card1 = Card(type: .spades, value: .four)
        let card2 = Card(type: .clubs, value: .four)
        let card3 = Card(type: .hearts, value: .four)
        let card4 = Card(type: .diamond, value: .four)
        let card5 = Card(type: .spades, value: .five)
        let card6 = Card(type: .diamond, value: .five)
        let card7 = Card(type: .clubs, value: .five)
        let card8 = Card(type: .hearts, value: .five)
        let card9 = Card(type: .diamond, value: .three)
        
        let cards: [Card] = [card5,card2,card3,card4,card1,card6,card7,card8,card9]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card9,card4,card6,card1,card2,card3,card5,card7,card8]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioNine() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .spades, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .spades, value: .four)
        let card5 = Card(type: .diamond, value: .ace)
        let card6 = Card(type: .diamond, value: .two)
        let card7 = Card(type: .diamond, value: .three)
        let card8 = Card(type: .hearts, value: .ace)
        let card9 = Card(type: .diamond, value: .four)
        let cards: [Card] = [card5,card2,card3,card4,card1,card6,card7,card8,card9]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card6,card7,card9,card2,card3,card4,card5,card1,card8]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioTenFullSequential() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .two)
        let card3 = Card(type: .clubs, value: .three)
        let card4 = Card(type: .clubs, value: .four)
        let card5 = Card(type: .clubs, value: .five)
        let card6 = Card(type: .clubs, value: .six)
        let card7 = Card(type: .clubs, value: .seven)
        let card8 = Card(type: .clubs, value: .eight)
        let card9 = Card(type: .clubs, value: .nine)
        let card10 = Card(type: .clubs, value: .ten)
        let card11 = Card(type: .clubs, value: .jack)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = smartCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        
        XCTAssert(result == expectation)
    }
}
