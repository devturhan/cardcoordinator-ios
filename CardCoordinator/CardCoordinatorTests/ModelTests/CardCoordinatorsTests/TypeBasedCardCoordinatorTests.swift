//
//  TypeBasedCardCoordinatorTests.swift
//  CardCoordinatorTests
//
//  Created by Ömer Turhan on 12.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import XCTest
@testable import CardCoordinator
class TypeBasedCardCoordinatorTests: XCTestCase {
    
    var typeBasedCardCoordinator:TypeBasedCardCoordinator!
    
    override func setUp() {
        super.setUp()
        typeBasedCardCoordinator = TypeBasedCardCoordinator()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        typeBasedCardCoordinator = nil
        super.tearDown()
    }
    
    func testCardCoordinatorWithEmptyArray() {
        let cards: [Card] = []
        let result = typeBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = []
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioOne() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .jack)
        let card3 = Card(type: .clubs, value: .three)
        let card4 = Card(type: .clubs, value: .four)
        let card5 = Card(type: .spades, value: .ace)
        let card6 = Card(type: .diamond, value: .four)
        let card7 = Card(type: .hearts, value: .ace)
        let card8 = Card(type: .diamond, value: .ace)
        let card9 = Card(type: .spades, value: .two)
        let card10 = Card(type: .clubs, value: .two)
        let card11 = Card(type: .diamond, value: .two)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = typeBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card1,card5,card7,card8,card9,card10,card11,card3,card4,card6,card2]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioTwo() {
        let card1 = Card(type: .clubs, value: .king)
        let card2 = Card(type: .diamond, value: .king)
        let card3 = Card(type: .spades, value: .king)
        let card4 = Card(type: .hearts, value: .king)
        let card5 = Card(type: .spades, value: .queen)
        let card6 = Card(type: .diamond, value: .queen)
        let card7 = Card(type: .hearts, value: .queen)
        let card8 = Card(type: .clubs, value: .jack)
        let card9 = Card(type: .spades, value: .jack)
        let card10 = Card(type: .hearts, value: .jack)
        let card11 = Card(type: .diamond, value: .jack)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = typeBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card8, card9, card10, card11, card5, card6, card7, card1, card2, card3, card4]
        
        XCTAssert(result == expectation)
    }
    
    func testCardCoordinatorWithScenarioThree() {
        let card1 = Card(type: .clubs, value: .king)
        let card2 = Card(type: .diamond, value: .king)
        let card3 = Card(type: .spades, value: .king)
        let card4 = Card(type: .hearts, value: .king)
        let card5 = Card(type: .spades, value: .queen)
        let card6 = Card(type: .diamond, value: .queen)
        let card7 = Card(type: .hearts, value: .queen)
        let card8 = Card(type: .clubs, value: .jack)
        let card9 = Card(type: .spades, value: .jack)
        let card10 = Card(type: .hearts, value: .jack)
        let card11 = Card(type: .diamond, value: .jack)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let result = typeBasedCardCoordinator.coordinateCards(cards: cards)
        let expectation: [Card] = [card8, card9, card10, card11, card5, card6, card7, card1, card2, card3, card4]
        
        XCTAssert(result == expectation)
    }
    
}
