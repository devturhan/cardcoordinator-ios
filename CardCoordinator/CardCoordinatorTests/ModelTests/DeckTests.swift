//
//  DeckTests.swift
//  CardCoordinatorTests
//
//  Created by Ömer Turhan on 15.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import XCTest
@testable import CardCoordinator
class DeckTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testDeckShuffle() {
        let card1 = Card(type: .spades, value: .ace)
        let card2 = Card(type: .spades, value: .two)
        let card3 = Card(type: .spades, value: .three)
        let card4 = Card(type: .spades, value: .four)
        let card5 = Card(type: .diamond, value: .three)
        let card6 = Card(type: .diamond, value: .four)
        let card7 = Card(type: .diamond, value: .five)
        let card8 = Card(type: .hearts, value: .ace)
        let card9 = Card(type: .diamond, value: .ace)
        let card10 = Card(type: .clubs, value: .four)
        let card11 = Card(type: .hearts, value: .four)
        
        let cards: [Card] = [card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11]
        let deck = Deck(cards: cards)
        deck.shuffleDeck()
        var areCardsOnSameOrder = true
        for index in 0..<deck.cards.count {
            let card = deck.cards[index]
            if card != cards[index]{
                areCardsOnSameOrder = false
                break
            }
        }
        XCTAssert(!areCardsOnSameOrder)
    }
}
