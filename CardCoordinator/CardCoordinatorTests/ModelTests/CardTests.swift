//
//  CardTests.swift
//  CardCoordinatorTests
//
//  Created by Ömer Turhan on 15.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import XCTest
@testable import CardCoordinator

class CardTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCardEquality() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .ace)
        XCTAssert(card1 == card2)
    }
    
    func testCardNotEqualityWithDifferentType() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .diamond, value: .ace)
        XCTAssert(card1 != card2)
    }
    
    func testCardNotEqualityWithDifferentValue() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .jack)
        XCTAssert(card1 != card2)
    }
    
    func testCardValueComparison() {
        let card1 = Card(type: .clubs, value: .ace)
        let card2 = Card(type: .clubs, value: .jack)
        XCTAssert(card1 < card2)
    }
    
}
