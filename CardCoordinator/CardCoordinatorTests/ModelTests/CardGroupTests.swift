//
//  CardGroupTests.swift
//  CardCoordinatorTests
//
//  Created by Ömer Turhan on 15.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import XCTest
@testable import CardCoordinator
class CardGroupTests: XCTestCase {
    
    var cardGroup : CardGroup!
    
    override func setUp() {
        super.setUp()
        cardGroup = CardGroup(groupType: .sequentialBased, cards: [Card(type: .diamond, value: .jack),
                                                                   Card(type: .diamond, value: .queen),
                                                                   Card(type: .diamond, value: .king)])
    }
    
    override func tearDown() {
        cardGroup = nil
        super.tearDown()
    }
    
    func testCardGroupPointSequentialGroup() {
        cardGroup = CardGroup(groupType: .sequentialBased, cards: [Card(type: .diamond, value: .jack),
                                                                   Card(type: .diamond, value: .queen),
                                                                   Card(type: .diamond, value: .king)])
        let expectedPoint = 30
        XCTAssert(expectedPoint == cardGroup.cardTotalPoint)
    }
    
    func testCardGroupPointTypeBasedGroup() {
        cardGroup = CardGroup(groupType: .typeBased, cards: [Card(type: .diamond, value: .jack),
                                                             Card(type: .clubs, value: .jack),
                                                             Card(type: .hearts, value: .jack)])
        let expectedPoint = 30
        XCTAssert(expectedPoint == cardGroup.cardTotalPoint)
    }
    
    
    func testCardGroupPointNoneGroup() {
        cardGroup = CardGroup(groupType: .none, cards: [Card(type: .diamond, value: .jack),
                                                        Card(type: .clubs, value: .six),
                                                        Card(type: .hearts, value: .ace)])
        let expectedPoint = 0
        XCTAssert(expectedPoint == cardGroup.cardTotalPoint)
    }
}
