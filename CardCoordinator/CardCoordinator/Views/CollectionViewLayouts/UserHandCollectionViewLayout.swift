//
//  UserHandCollectionViewLayout.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 13.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import UIKit

class UserHandCollectionViewLayout: UICollectionViewFlowLayout {
    
    var cachedAttributes : [UserHandCollectionViewCustomAttributes]?
    
    let itemCount = 11
    let maxHeightDifferenceBetweenElements:CGFloat = 40
    let radius:CGFloat = 300
    
    var angleAnchorPoint: CGPoint?
    var midItemCenter: CGPoint?
    
    var firstItemAttribute : UserHandCollectionViewCustomAttributes?
    override func prepare() {
        super.prepare()
        minimumInteritemSpacing = 0
        itemSize = CGSize(width: 50, height: 80)
    }
    
    override class var layoutAttributesClass: AnyClass {
        return UserHandCollectionViewCustomAttributes.self
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cachedAttributes?[indexPath.row]
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let attributes = super.layoutAttributesForElements(in: rect) as? [UserHandCollectionViewCustomAttributes] else { return nil}
        if attributes.count == 0 {
            return attributes
        }
        
        //Coping Attributes
        var copiedAttributes = attributes.map { (attribute) -> UserHandCollectionViewCustomAttributes in
            if let copiedAttribute = attribute.copy() as? UserHandCollectionViewCustomAttributes {
                return copiedAttribute
            }
            fatalError("Attribute copy failed on layoutAttributesForElements")
        }
        
        configureFrames(attributes: &copiedAttributes)
        configureAnglesForRotation(attributes: &copiedAttributes)
        cachedAttributes = copiedAttributes
        
        return copiedAttributes
    }
    
    func configureFrames( attributes:inout [UserHandCollectionViewCustomAttributes]){
        let halfOfItemCount = itemCount / 2
        attributes = attributes.enumerated().map { index, attribute -> UserHandCollectionViewCustomAttributes in
            var calculatedY = attribute.frame.origin.y
            if index < halfOfItemCount {
                calculatedY = CGFloat(maxHeightDifferenceBetweenElements) / CGFloat(halfOfItemCount) * (CGFloat(halfOfItemCount) - CGFloat(index))
                //Increasing outer items' Y coordinate because their angle between mid element is higher than others
                calculatedY = calculatedY + CGFloat(halfOfItemCount - index) *
                    CGFloat(maxHeightDifferenceBetweenElements)/CGFloat(itemCount) / CGFloat(index + 1)
            }else if index == halfOfItemCount {
                calculatedY = CGFloat(maxHeightDifferenceBetweenElements) / CGFloat(halfOfItemCount) * (CGFloat(halfOfItemCount) - CGFloat(index)) + 5
            }else {
                calculatedY = CGFloat(maxHeightDifferenceBetweenElements) / CGFloat(halfOfItemCount) * (CGFloat(index) - CGFloat(halfOfItemCount))
                //Increasing outer items' Y coordinate because their angle between mid element is higher than others
                calculatedY = calculatedY + CGFloat(index - halfOfItemCount ) *
                    CGFloat(maxHeightDifferenceBetweenElements)/CGFloat(itemCount) / CGFloat(itemCount-index)
            }
            
            let spacing =  CGFloat(index) * attribute.frame.size.width / 2
            attribute.frame = CGRect(x: attribute.frame.origin.x + 30 - spacing * 2/4 ,
                                     y: calculatedY + 5,
                                     width: attribute.frame.size.width,
                                     height: attribute.frame.size.height)
            if index == halfOfItemCount {
                angleAnchorPoint = CGPoint(x: attribute.frame.origin.x + attribute.frame.size.width/2, y: -radius)
                midItemCenter = attribute.center
            }
            if index == 0 {
                firstItemAttribute = attribute
            }
            return attribute
        }
    }
    
    func configureAnglesForRotation( attributes:inout [UserHandCollectionViewCustomAttributes]){
        guard let angleAnchorPoint = angleAnchorPoint else { fatalError() }
        guard let midItemCenter = midItemCenter else { fatalError() }
        attributes = attributes.enumerated().map({ index, attribute -> UserHandCollectionViewCustomAttributes in
            let v1 = CGVector(dx: midItemCenter.x - angleAnchorPoint.x, dy: midItemCenter.y - angleAnchorPoint.y)
            let v2 = CGVector(dx: attribute.center.x - angleAnchorPoint.x, dy: attribute.center.y - angleAnchorPoint.y)
            
            let angle = atan2(v1.dy, v1.dx) - atan2(v2.dy, v2.dx)
            attribute.angle = angle
            return attribute
        })
    }
    
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UserHandCollectionViewCustomAttributes(forCellWith: itemIndexPath)
        if let collectionView = collectionView {
            let x = collectionView.bounds.width - itemSize.width
            let y = collectionView.bounds.midY
            attributes.size = self.itemSize
            attributes.center = CGPoint(x: x, y: y)
        }
        
        return attributes
    }
    
    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UserHandCollectionViewCustomAttributes(forCellWith: itemIndexPath)
        if let collectionView = collectionView {
            let x = collectionView.bounds.width - itemSize.width 
            let y = collectionView.bounds.midY
            attributes.size = self.itemSize
            attributes.center = CGPoint(x: x, y: y)
        }
        return attributes
    }
    
}

class UserHandCollectionViewCustomAttributes: UICollectionViewLayoutAttributes {
    var angle: CGFloat = 0 {
        didSet {
            transform = CGAffineTransform(rotationAngle: angle)
        }
    }
    override func copy(with zone: NSZone? = nil) -> Any {
        if let copiedAtrributes = super.copy(with: zone) as? UserHandCollectionViewCustomAttributes {
            copiedAtrributes.angle = angle
            return copiedAtrributes
        }
        fatalError("copy")
    }
}
