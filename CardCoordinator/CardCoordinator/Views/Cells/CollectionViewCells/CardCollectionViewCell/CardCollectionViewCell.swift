//
//  CardCollectionViewCell.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 12.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import UIKit

class CardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cardImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
    }
}
