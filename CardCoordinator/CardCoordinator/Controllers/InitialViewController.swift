//
//  ViewController.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    @IBOutlet weak var userHandCollectionView: UICollectionView!
    
    var userHand : UserHand?
    var dataSourceCards : Cards = []
    var deck: Deck!

    override func viewDidLoad() {
        super.viewDidLoad()
        createDeck()
        configureUserHandCollectionView()
    }
    
    func createDeck(){
        let cardGenerator = CardGenerator()
        deck = Deck(cards: cardGenerator.generateCards(for: .standard52))
    }

    func configureUserHandCollectionView() {
        let nib = UINib(nibName: "CardCollectionViewCell", bundle: Bundle(for: CardCollectionViewCell.self))
        userHandCollectionView.register(nib, forCellWithReuseIdentifier: "CardCollectionViewCell")
        userHandCollectionView.dataSource = self
    }
    
    func createNewUserHand(){
        userHand = UserHand(cards: deck.getRandomElevenCards())
    }
    
    @IBAction func getNewHandButtonClicked(_ sender: Any) {
        removeAllCardsWithAnimation { [weak self] in
            guard let unwrappedSelf = self else { return }
            unwrappedSelf.createNewUserHand()
            
            guard let unwrappedUserHand = unwrappedSelf.userHand else { return }
            unwrappedSelf.dataSourceCards = unwrappedUserHand.getCards()
            
            unwrappedSelf.reloadCollectionViewSectionWithAnimation(section: 0) {}
        }
    }
    
    @IBAction func sequentialCoordinationButtonClicked(_ sender: Any) {
        handleCoordinationButtonClick(cardCoordinatorType: .sequentialBased)
    }
    
    @IBAction func typeBasedCoordinationButtonClicked(_ sender: Any) {
        handleCoordinationButtonClick(cardCoordinatorType: .typeBased)
    }
    
    @IBAction func smartCoordinationButtonClicked(_ sender: Any) {
        handleCoordinationButtonClick(cardCoordinatorType: .smart)
    }
    
    func handleCoordinationButtonClick(cardCoordinatorType : CardCoordinatorType){
        
        removeAllCardsWithAnimation { [weak self] in
            guard let unwrappedSelf = self else { return }
            if let userHand = unwrappedSelf.userHand {
                userHand.coordinateCards(cardCoordinator:cardCoordinatorType.getCoordinator())
                unwrappedSelf.dataSourceCards = userHand.getCards()
                unwrappedSelf.reloadCollectionViewSectionWithAnimation(section: 0) {}
            }
        }
    }
    
    func removeAllCardsWithAnimation(onCompletion: @escaping () -> Void) {
        dataSourceCards.removeAll()
        reloadCollectionViewSectionWithAnimation(section: 0) { onCompletion() }
    }
    
    func reloadCollectionViewSectionWithAnimation(section: Int, onCompletion: @escaping () -> Void) {
        UIView.animate(withDuration: 1, animations: { [weak self] in
            guard let unwrappedSelf = self else { return }
            unwrappedSelf.userHandCollectionView.performBatchUpdates({ [weak self] in
                guard let unwrappedSelf = self else { return }
                unwrappedSelf.userHandCollectionView.reloadSections(NSIndexSet(index: section) as IndexSet)
            }, completion: { (finished:Bool) -> Void in
                onCompletion()
            })
        })
    }
}

extension InitialViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSourceCards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardCollectionViewCell", for: indexPath) as? CardCollectionViewCell else { fatalError("InitialViewController cellForItemAt method cell dequeue failed.")}
        let card = dataSourceCards[indexPath.row]
        cell.cardImageView.image = UIImage(named:"\(card.value.rawValue)_of_\(card.type.rawValue)")
        cell.clipsToBounds = true
        cell.layer.cornerRadius = 6
        return cell
    }
}
