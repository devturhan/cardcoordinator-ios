//
//  ArrayExtensions.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

extension Array {
    func group<T>(by criteria: (Element) -> T) -> [T: [Element]] {
        var groups = [T: [Element]]()
        
        for element in self {
            let key = criteria(element)
            if groups.keys.contains(key) == false {
                groups[key] = [Element]()
            }
            groups[key]?.append(element)
        }
        return groups
    }
}

extension Array where Element: Equatable {
    mutating func remove(object: Element) {
        if let index = index(of: object) {
            remove(at: index)
        }
    }
}
