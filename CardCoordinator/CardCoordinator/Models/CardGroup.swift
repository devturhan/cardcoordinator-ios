//
//  CardGroup.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

typealias CardGroups = [CardGroup]

enum CardGroupType {
    case sequentialBased
    case typeBased
    case none
}

class CardGroup {
    let groupType : CardGroupType
    let cards: Cards
    
    lazy var cardTotalPoint = { [unowned self] in
        return self.getGroupPoint()
    }()
    
    init(groupType:CardGroupType, cards:[Card]) {
        self.groupType = groupType
        self.cards = cards
    }
    
    private func getGroupPoint() -> Int{
        if groupType == .none {
            return 0
        }
        
        var point = 0
        for card in cards {
            point += card.point
        }
        return point
    }
    
    func getCards() -> Cards {
        return cards
    }
}
