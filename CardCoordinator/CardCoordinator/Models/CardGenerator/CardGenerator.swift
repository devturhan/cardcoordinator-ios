//
//  CardGenerator.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

struct CardGenerator {
    
    func generateCards(for deckType: DeckType)-> [Card] {
        var cards: [Card] = []
        switch deckType {
        case .standard52:
            for type in CardType.getAllTypes() {
                for value in CardValue.getAllValues() {
                    cards.append(Card(type: type, value: value))
                }
            }
        }
        return cards
    }
}
