//
//  UserHand.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

class UserHand {
    private var cards: [Card]
    
    init(cards: [Card]){
        self.cards = cards
    }
    
    func getCards() -> [Card] {
        return cards
    }
    
    func coordinateCards(cardCoordinator:CardCoordinator){
        self.cards = cardCoordinator.coordinateCards(cards: cards)
    }
}
