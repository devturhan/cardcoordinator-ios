//
//  Deck.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

enum DeckType {
    case standard52
}

class Deck {
    
    var cards : [Card]
    
    init(cards:[Card]) {
        self.cards = cards
    }
    
    func getRandomElevenCards()->[Card] {
        if cards.count < 11 { fatalError("Deck does not contain eleven or more cards.") }
        var tempCards = cards
        var randomElevenCards:[Card] = []

        for _ in 0..<11 {
            let rand = Int(arc4random_uniform(UInt32(tempCards.count)))
            randomElevenCards.append(tempCards[rand])
            tempCards.remove(at: rand)
        }
        
        return randomElevenCards
    }
    
    func shuffleDeck() {
        var tempCards = cards
        var newCards: [Card] = []
        for _ in 0..<cards.count{
            let rand = Int(arc4random_uniform(UInt32(tempCards.count)))
            newCards.append(tempCards[rand])
            tempCards.remove(at: rand)
        }
        self.cards = newCards
    }
}
