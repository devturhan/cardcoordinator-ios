//
//  SortAlgorithm.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

protocol SortAlgorithm {
    func sort<T: Comparable>(_ array: [T]) -> [T] 
}
