//
//  SequentialCardCoordinator.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

class SequentialBasedCardCoordinator: CardCoordinator {
    
    func createCardGroups(cards: Cards) -> CardGroups {
        let sortedCards = self.sortAlgorithm.sort(cards)
        let groupedAndSortedCards = sortedCards.group { (card) -> CardType in
            return card.type
        }
        var cardGroups: CardGroups = []
        
        for cardType in CardType.getAllTypes() {
            if let typedSortedCards = groupedAndSortedCards[cardType] {
                let tempCardGroups = getCardGroups(sortedCards: typedSortedCards)
                _ = tempCardGroups.map{cardGroups.append($0)}
            }
        }
        
        return cardGroups
    }
    
    
    func coordinateCards(cards: Cards) -> Cards {
       
        let cardGroups = createCardGroups(cards: cards)
        var coordinatedCardList: Cards = []
        
        let consecutiveCards = cardGroups.compactMap { (cardGroup) -> Cards? in
            if cardGroup.groupType == .sequentialBased { return cardGroup.cards }
            return nil
        }.flatMap{ $0 }
        
        let nonConsecutiveCard = cardGroups.compactMap { (cardGroup) -> Cards? in
            if cardGroup.groupType == .none { return cardGroup.cards }
            return nil
        }.flatMap{ $0 }
        
        coordinatedCardList = consecutiveCards
        coordinatedCardList.append(contentsOf: nonConsecutiveCard)
        
        return coordinatedCardList
    }
    
    func getCardGroups(sortedCards: Cards) -> CardGroups {
        var cardGroups: CardGroups = []
        var tempCardArr: Cards = []
        var nonconsecutiveCards: Cards = sortedCards
        
        var currentIndex = 0
        var nextIndex = currentIndex + 1
        
        while nextIndex < sortedCards.count {
            if (sortedCards[currentIndex].value.getCardOrderNumber() == sortedCards[nextIndex].value.getCardOrderNumber() - 1) {
                tempCardArr.append(sortedCards[currentIndex])
                
                if (nextIndex + 1 == sortedCards.count){ // Last Index
                    tempCardArr.append(sortedCards[nextIndex])
                }
                
            } else {
                if tempCardArr.count > 0 {
                    tempCardArr.append(sortedCards[currentIndex])
                }
                
                if tempCardArr.count >= 3 {
                    let cardGroup = CardGroup(groupType: .sequentialBased, cards: tempCardArr)
                    nonconsecutiveCards = nonconsecutiveCards.filter { return !tempCardArr.contains($0)}
                    cardGroups.append(cardGroup)
                }
                
                tempCardArr = []
                
            }
            
            currentIndex += 1
            nextIndex += 1
        }
        
        if (tempCardArr.count >= 3){
            let cardGroup = CardGroup(groupType: .sequentialBased, cards: tempCardArr)
            nonconsecutiveCards = nonconsecutiveCards.filter { return !tempCardArr.contains($0)}
            cardGroups.append(cardGroup)
        }
        
        if nonconsecutiveCards.count > 0 {
            let nonConsecutiveCardGroups = CardGroup(groupType: .none, cards: nonconsecutiveCards)
            cardGroups.append(nonConsecutiveCardGroups)
        }
        
        return cardGroups
    }
}


