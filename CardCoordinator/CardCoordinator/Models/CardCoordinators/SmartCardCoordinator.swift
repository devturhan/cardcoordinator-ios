//
//  SmartCardCoordinator.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

class SmartCardCoordinator: CardCoordinator {
    
    func coordinateCards(cards: Cards) -> Cards {
        let cardGroups = createCardGroups(cards: cards)
        var smartCardList: Cards = []
        
        let consecutiveCards = cardGroups.compactMap { (cardGroup) -> Cards? in
            if cardGroup.groupType == .sequentialBased { return cardGroup.cards }
            return nil
            }.flatMap{ $0 }
        
        let typeBasedCardList = cardGroups.compactMap { (cardGroup) -> Cards? in
            if cardGroup.groupType == .typeBased { return cardGroup.cards }
            return nil
            }.flatMap{ $0 }
        
        let nonRelatedCard = cardGroups.compactMap { (cardGroup) -> Cards? in
            if cardGroup.groupType == .none { return cardGroup.cards }
            return nil
            }.flatMap{ $0 }
        
        smartCardList = consecutiveCards
        smartCardList.append(contentsOf: typeBasedCardList)
        smartCardList.append(contentsOf: nonRelatedCard)
        
        return smartCardList
    }
    
    func createCardGroups(cards: Cards) -> CardGroups {
        let (calculatedGroup1, point1) = findMaximumTotalPointStartFromFirstCoordinatorToSecond(firstCardCoordinatorType: .sequentialBased,
                                                                                                secondCardCoordinatorType: .typeBased,
                                                                                                cards: cards)
        
        let (calculatedGroup2, point2) = findMaximumTotalPointStartFromFirstCoordinatorToSecond(firstCardCoordinatorType: .typeBased,
                                                                                                secondCardCoordinatorType: .sequentialBased,
                                                                                                cards: cards)
        
        return point1 >= point2 ? calculatedGroup1 : calculatedGroup2
    }
    
    func findMaximumTotalPointStartFromFirstCoordinatorToSecond (firstCardCoordinatorType:CardCoordinatorType,
                                                                 secondCardCoordinatorType:CardCoordinatorType,
                                                                 cards: Cards) -> (CardGroups,Int) {
        var calculatedMaximumPoint = -1
        var maximumPointCardGroups : CardGroups = []
        
        let firstCardCoordinator = firstCardCoordinatorType.getCoordinator()
        let secondCardCoordinator = secondCardCoordinatorType.getCoordinator()
        
        let firstCardCoordinatorCardGroupsWithNones = firstCardCoordinator.createCardGroups(cards: cards)
        let firstCardCoordinatorResultsWithoutNones = firstCardCoordinatorCardGroupsWithNones.filter { (cardGroup) -> Bool in
            return cardGroup.groupType == firstCardCoordinatorType.getCardGroupType()
        }
        
        let firstCardCoordinatorResultsOnlyNonesCGs = firstCardCoordinatorCardGroupsWithNones.filter { (cardGroup) -> Bool in
            return cardGroup.groupType == .none
        }
        
        if firstCardCoordinatorResultsWithoutNones.count == 0 {
            let secondCardCoordinatorCreatedGroups = secondCardCoordinator.createCardGroups(cards: cards)
            let point = calculateTotalPoint(for: secondCardCoordinatorCreatedGroups)
            return (secondCardCoordinatorCreatedGroups, point)
        }
        
        //Passing first coordinator result of not related cards to second Coordinator
        let firstCoordinatorResultNoneCards = firstCardCoordinatorResultsOnlyNonesCGs.flatMap { $0.getCards() }
        let secondCoordinatorResultCGs = secondCardCoordinator.createCardGroups(cards: firstCoordinatorResultNoneCards)
        
        //Combine FirstAlgorithm Result with second Algortihm Result
        var tempCardGroups = secondCoordinatorResultCGs
        tempCardGroups.append(contentsOf: firstCardCoordinatorResultsWithoutNones)
        let combineCardGroups = tempCardGroups
        let combinedCardGroupsTotalPoint = calculateTotalPoint(for: combineCardGroups)
        
        if calculatedMaximumPoint < combinedCardGroupsTotalPoint {
            maximumPointCardGroups = combineCardGroups
            calculatedMaximumPoint = combinedCardGroupsTotalPoint
        }
        
        // Passing each element in first card coodinator to second card coordinator with first card coordinator nones to find better total point
        let firstCardCoordinatorCardsWithoutUnrelateds = firstCardCoordinatorCardGroupsWithNones
            .filter { $0.groupType == firstCardCoordinatorType.getCardGroupType() }
            .flatMap { $0.getCards() }
        
        let firstCardCoordinatorUnrelatedCards = firstCardCoordinatorCardGroupsWithNones
            .filter { $0.groupType != firstCardCoordinatorType.getCardGroupType() }
            .flatMap { $0.getCards() }
        
        for card in firstCardCoordinatorCardsWithoutUnrelateds {
            //Removing card from first card coordinator items list
            var tempfirstCardCoordinatorCardsWithoutUnrelateds = firstCardCoordinatorCardsWithoutUnrelateds
            tempfirstCardCoordinatorCardsWithoutUnrelateds.remove(object: card)
            
            //Calculate new point for first algorithm without removed card
            let newFirstCardCoordinatorCardGroupsWithNones = firstCardCoordinator.createCardGroups(cards: tempfirstCardCoordinatorCardsWithoutUnrelateds)
            let newFirstCardCoordinatorCardGroupsPoint = calculateTotalPoint(for: newFirstCardCoordinatorCardGroupsWithNones)
            
            //Adding removed card to firstCardCoordinatorNotRelatedCards array and passing it to second algorithm
            var tempNonSequentialCards = firstCardCoordinatorUnrelatedCards
            tempNonSequentialCards.append(card)
            
            let secondCardCoordinatorResultWithNones = secondCardCoordinator.createCardGroups(cards: tempNonSequentialCards)
            let secondCardCoordinatorResultsCardGroupPoint = calculateTotalPoint(for: secondCardCoordinatorResultWithNones)
            
            let calculatedPoint = newFirstCardCoordinatorCardGroupsPoint + secondCardCoordinatorResultsCardGroupPoint
            
            if calculatedMaximumPoint < calculatedPoint {
                var tempCombinedCardGroups = newFirstCardCoordinatorCardGroupsWithNones
                tempCombinedCardGroups.append(contentsOf: secondCardCoordinatorResultWithNones)
                calculatedMaximumPoint = calculatedPoint
                maximumPointCardGroups = tempCombinedCardGroups
            }
        }
        
        return (maximumPointCardGroups, calculatedMaximumPoint)
    }
    
    
    func calculateTotalPoint(for cardGroups: [CardGroup]) -> Int {
        let filteredCardGroups = cardGroups.filter { $0.groupType != .none }
        let point = filteredCardGroups.flatMap{ $0.getCards() }.map{ $0.point }.reduce(0, +)
        return point
    }
    
}
