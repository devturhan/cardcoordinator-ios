//
//  CardCoordinator.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

enum CardCoordinatorType {
    case sequentialBased
    case typeBased
    case smart
    
    func getCoordinator()->CardCoordinator {
        switch self {
        case .sequentialBased:
            return SequentialBasedCardCoordinator()
        case .typeBased:
            return TypeBasedCardCoordinator()
        case .smart:
            return SmartCardCoordinator()
        }
    }
    
    func getCardGroupType()-> CardGroupType {
        switch self {
        case .sequentialBased:
            return CardGroupType.sequentialBased
        case .typeBased:
            return CardGroupType.typeBased
        default:
            fatalError("There is no card group type for other card coordinator types")
        }
    }
}

protocol CardCoordinator {
    var sortAlgorithm: SortAlgorithm { get }
    func coordinateCards(cards: Cards) -> Cards
    func createCardGroups(cards:Cards) -> CardGroups
}

extension CardCoordinator {
    var sortAlgorithm: SortAlgorithm {
        get {
            return MergeSort()
        }
    }
}
