//
//  TypeBasedCardCoordinator.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

class TypeBasedCardCoordinator: CardCoordinator {
    func createCardGroups(cards: Cards) -> CardGroups {
        var cardGroups: CardGroups = []
        
        let groupedByValueCards = cards.group { (card) -> CardValue in
            return card.value
        }
        
        for value in CardValue.getAllValues(){
            if let sameValueCards = groupedByValueCards[value] {
                if sameValueCards.count > 2 {
                    cardGroups.append(CardGroup(groupType: .typeBased, cards: sameValueCards))
                } else {
                    cardGroups.append(CardGroup(groupType: .none, cards: sameValueCards))
                }
            }
        }
        return cardGroups
    }
    
    func coordinateCards(cards: Cards) -> Cards {
        let getCardGroups = createCardGroups(cards: cards)
        var coordinatedCardList: Cards = []
        
        let typeBasedCombinedCards = getCardGroups.compactMap { (cardGroup) -> Cards? in
            if cardGroup.groupType == .typeBased { return cardGroup.cards }
            return nil
            }.flatMap{ $0 }
        
        let nonRelatedCards = getCardGroups.compactMap { (cardGroup) -> Cards? in
            if cardGroup.groupType == .none { return cardGroup.cards }
            return nil
            }.flatMap{ $0 }
        
        coordinatedCardList = typeBasedCombinedCards
        coordinatedCardList.append(contentsOf: nonRelatedCards)
        
        return coordinatedCardList
    }
}
