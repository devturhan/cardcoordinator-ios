//
//  CardPrinter.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

import Foundation

protocol CardPrinter {
    var cards: [Card] { get }
    func printCardsToConsole()
}

extension CardPrinter {
    func printCardsToConsole(){
        for card in cards {
            print("\(card.value) \(card.type)")
        }
    }
}
