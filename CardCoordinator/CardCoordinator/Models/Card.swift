//
//  Card.swift
//  CardCoordinator
//
//  Created by Ömer Turhan on 11.07.2018.
//  Copyright © 2018 Ömer Turhan. All rights reserved.
//

typealias Cards = [Card]

enum CardType: String {
    case diamond 
    case spades
    case hearts
    case clubs
    
    static func getAllTypes() -> [CardType] {
        return [.diamond, .spades, .clubs, .hearts]
    }
    
}

enum CardValue: Int {
    case ace = 1
    case two = 2
    case three = 3
    case four = 4
    case five = 5
    case six = 6
    case seven = 7
    case eight = 8
    case nine = 9
    case ten = 10
    case jack = 11
    case queen = 12
    case king = 13
    
    fileprivate func getPoint() -> Int {
        switch self {
        case .king:
            return 10
        case .queen:
            return 10
        case .jack:
            return 10
        default:
            return self.rawValue
        }
    }
    
    static func getAllValues() -> [CardValue] {
        return [.ace, .two , .three, .four , .five, .six, .seven, .eight, .nine, .ten, .jack, .queen, .king]
    }
    
    func getCardOrderNumber()-> Int{
        return self.rawValue
    }
    
}

class Card: Equatable, Comparable{
 
    let type: CardType
    let value: CardValue
    
    lazy var point = {
       return value.getPoint()
    }()
    
    init(type : CardType, value : CardValue) {
        self.type = type
        self.value = value
    }
    
    static func == (lhs: Card, rhs: Card) -> Bool {
        return lhs.type == rhs.type && lhs.value == rhs.value
    }
    
    static func < (lhs: Card, rhs: Card) -> Bool {
        if(lhs.value.rawValue < rhs.value.rawValue){
            return true
        }
        return false
    }
    
}
